using System;

namespace Task3
{
    public class Cars
    {
        protected double speed = 0;//всё что "protected" - защищаем переменные от изменения снаружи(используя свойства get set), тут я еще не до конца разобрался, но так надо.
        public double _speed
        {
            get {return speed;}
            set {speed = value;}                    
        }
        protected double price = 0;
        public double _price
        {
            get {return price;}
            set {price = value;}
        }
        protected string transmission = "";
        public string _transmission
        {
            get {return transmission;}
            set {transmission = value;}
        }
        protected string carName = "";
        public string _carName
        {
            get {return carName;}
            set {carName = value;}
        }
         
                        

        public static void Description(double speed, double price, string carName, string transmission)
        {
            Console.WriteLine($"Марка: {carName}; \nМаксимальная скорость: {speed}км/ч; \nКоробка передач: {transmission}; \nЦена: {price}$\n ");
        }
        public static void Poexali(string p) => Console.WriteLine(p);

    }
}