﻿using System;

namespace Task3
{
	public class Program
	{
		 public static void Main()
		{
			
			Jiguli jiguli = new Jiguli {_speed = 160, _price = 4000, _transmission = "Механика", _carName = "Жигули"};//создаю два объекта классов Жигули и мицубиши
			Mitsubishi mitsu = new Mitsubishi{_speed = 180, _price = 7000, _transmission = "Автомат", _carName = "Мицубиши Кольт"};

			
			List<string> spisokAuto = new List<string>();//создаю коллекцию (список, типа массива)											
			spisokAuto.Add("Жигуль");//добавляю авто в список(можно расширить)
			spisokAuto.Add("Мицубиши");
						

			Console.WriteLine("В нашем автосалоне \"10-е колесо\" представлены следующие автомобили:");
			foreach(var i in spisokAuto)//выводит список, если список изменится - будет выводить все авто(в том числе и добавленные)
				{
					Console.WriteLine(i);
				}

				
			try//ловим исключения			
			{	
				Console.WriteLine(" "); //все пустые строки для красивости и нормального восприятия текста в программе
				Console.WriteLine("Пожалуйста выберите автомобиль:\nДля выбора автомобиля марки \"Жигуль\" введите цифру 1. \nДля выбора автомобиля марки \"Мицубиши\" введите цифру 2.");
				int key = Convert.ToInt32(Console.ReadLine());//считываем данные консольного ввода, конвертируем в int для последующего сравнения
							
				switch (key)//сравниваем значения
					{
						case 1:	//выполняется код для жигулей	
							Jiguli.Description(160, 4000, "ВАЗ-2106", "Механика");//вызываем родительский метод, выводим описание авто
							Console.WriteLine(" ");													
							Console.WriteLine("Ну что, поехали?");
							Console.WriteLine("Нажмите Enter чтобы поехать.");
							Console.ReadLine();
							Cars.Poexali("Тыр-тыр-тыр");//родительский метод "Поехали"
						break;
						case 2://выполняется код для мицубиши
							Mitsubishi.Description(180, 7000, "Mitsubishi Colt", "Автомат");
							Console.WriteLine(" ");
							Console.WriteLine("Ну что, поехали?");
							Console.WriteLine("Нажмите Enter чтобы поехать.");
							Console.ReadLine();
							Cars.Poexali("Вжух!!!");
						break;

						default:
							Console.WriteLine("Ошибка. Авто с таким индексом еще не привезли.");//ловим ошибку по индексу в списке(неверно введенный индекс)
						break;		
					}
			}

			catch//поймали исключение =)
			{
				Console.WriteLine("Ошибка. Введены некоректные данные!");
			}

			Console.WriteLine(" ");			
			Console.WriteLine("Нажмите Enter чтобы завершить");
			Console.ReadLine();	
		}
	}
}